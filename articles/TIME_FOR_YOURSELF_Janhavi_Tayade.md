## TIME FOR YOURSELF
Janhavi Tayade

## para 1
“Time for yourself” simply means to take *some time off from your hectic schedule and do self-exploration.* In today’s world we are so lost in seeking perfection to please our parents,friends, teachers and many more people to whom we are unknown. But in this process we forget to think are we able to please ourselves.

## para 2
We always give our best to withstand in our surroundings. We try to adapt ourselves to the society so that we are not left behind. If we deeply analyze what actually happening is we are constantly changing ourselves as per the requirements of other people. We are unknowingly *losing our true self.* We are letting others manipulate us.

## para 3
We are so busy doing our tasks, that we are unable to take out even 1 second of rest from our lives. Even if we manage to take out that 1 second, instead of enjoying that 1 second we will think about the stuff we will be doing after that 1 second. In this way we keep on losing
such 1 seconds and at the end we regret not enjoying the time that we had earlier. We keep on sacrificing the things we like, to achieve our goals and once that goal is achieved we set-up another goal and try to achieve that and again we sacrifice our refreshments and this trap continues. We promise ourselves that once this goal is achieved, I’ll be satisfied with my life, but the real truth is that *we are never-ever satisfied*. It’s this human tendency that we keep on upgrading our bucket list to get something high. But why don’t we try to be happy in what we
possess right now.

## para 4
I’m not telling you not to dream big, but not to lose your true self in this process.Let’s take a very known example of a student aspiring to go to *IIT*. That student goes to the education hub of India-Kota and puts all his/her efforts and brain just to achieve that 1 seat in the renowned
institute.They forget their external life, they forget playing, spending time with friends, friends become competitors. What they know is to study 24*7 managing both schoolwork and coaching. If he/she gets that seat, then that day becomes the happiest one, but what if the scene is
opposite? After this the student thinks that what’s the use of sacrificing everything if he/she is unable to achieve the thing for which he/she worked day-and-night? Here the thing is not ofcourse to not focus on studies, but to make sacrifices to a very limited extent so that your regrets don’t consume you in your future. Even if you are not able to get IIT, you’ll still *cherish the memories* you made in those 2 years.

## para 5
Take out some time from studies and work,take your eyes off from laptop and mobile screens and chat with your childhood friends, *remember the old memories* which will make you laugh, remove your album which is lying in some corner of your home, spend some time with your elders, let the rhythm of music consume you, go out for a walk with your loved ones, plan a vacation not just to post stories on insta but to enjoy the moment, watch the sunset near the beach, write something you like, play your guitar or piano. Do whatever reveals the “true” you.

## para 6
Your *life is an one-time opportunity* so live it to the fullest instead of postponing the best moments of your life which may never come again.